# Wufoo Form Entry API  App

This app connects to a Wufoo form and allows a user to view the entries.

## Getting Started

Pull all files on the server and edit the /lib/config.php with the proper values for the form you'll be connecting to.

To filter the results to one specific ID when sending to an end user, add "?id=##" with the id of the entry you want to see replacing the ##.

## Built With

* [Jquery](https://jquery.com) - JS Library
* [DataTables](https://datatables.net) - Jquery library for table management and interaction
* [Materialize CSS](https://materializecss.com) - Style and Script library used for table styles and layout

## Authors

* **Jeremy Menking** [Email](jeremy.menking@gmail.com)