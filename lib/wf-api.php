<?php
namespace Wufoo\API;

class WFApi
{
    /**
     * The API URL base.
     * 
     * @var string
     */
    private $base;

    /**
     * Initializes properties.
     */
    public function __construct()
    {
        $this->setBase();
    }

    /**
     * Sets base property, or returns error on fail.
     *
     * @return mixed Optionally returns an error.
     */
    private function setBase()
    {
      $this->base =  'https://summerfitnessprogram.wufoo.com/api/v3/forms/'.FormId;
    }

    /**
     * Returns info about the form
     * 
     * @return mixed The decoded JSON or WP_Error.
     */
    public function getForm()
    {
      return $this->requestGet('.json', "");
    }


    /**
     * Returns the available fields of the form.
     * 
     * @return mixed The decoded JSON or WP_Error.
     */
    public function getFields()
    {
      return $this->requestGet('/fields.json', "");
    }

    /**
     * Returns the available fields of the form.
     * 
     * @return mixed The decoded JSON or WP_Error.
     */
    public function getEntries($id)
    {
      $filter = "";
      if ($id != "") $filter = '&Filter1=EntryId+Is_equal_to+'.$id;
      return $this->requestGet('/entries.json?pageSize=100&sort=EntryId&sortDirection=DESC'.$filter, "");
    }



    /**
     * Makes a GET request.
     *
     * @param string $endpoint The API endpoint.
     * @param array $params The query string parameters and values.
     * 
     * @return mixed The decoded JSON or WP_Error.
     */
    private function requestGet($endpoint, $params = [])
    {
      $query = '';
      
      $curl = curl_init();

      curl_setopt_array($curl, array(
      CURLOPT_URL => $this->base . $endpoint,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_ENCODING => "",
      CURLOPT_MAXREDIRS => 10,
      CURLOPT_TIMEOUT => 30,
      CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
      CURLOPT_CUSTOMREQUEST => "GET",
      CURLOPT_USERPWD => ApiKey.":footastic",
      CURLOPT_HTTPHEADER => array(
       "Cache-Control: no-cache",
      ),
      ));
    
      $response = curl_exec($curl);
      
      $err = curl_error($curl);
      curl_close($curl);
  
      return $this->buildResponse($response);
    }

    /**
     * Sets the response output.
     * 
     * @param string $request The JSON response body.
     * 
     * @return mixed The decoded JSON or WP_Error.
     */
    private function buildResponse($request)
    {
        $decoded = json_decode($request, true);

        if (isset($decoded->error_code) && (0 !== $decoded->error_code)) {
            $this->logError($decoded);
            $message = $this->filterErrorMessage($decoded->error_message);

            // no proper code returned, so we'll use 500
            $response = new WP_Error(500, $message, $decoded->data);
        } else {
            $response = $decoded;
        }

        return $response;
    }

    /**
     * Conditionally updates error message text.
     * 
     * @param string $message The existing error message.
     * 
     * @return string The updated error message.
     */
    private function filterErrorMessage($message)
    {
        
    }

    /**
     * Logs a new error.
     * 
     * @param mixed $message The error message.
     */
    private function logError($message)
    {

    }
}
