<?php 
  // Initialize config and api class
  require_once 'lib/init.php';
  use Wufoo\API\WFApi;  
  $wfApi = new WFApi(); 
?>

<html>
  <head>
    <title>Form Entries Managment</title>
    <link type="stylesheet" src="assets/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="assets/css/materialize.min.css">
    <link rel="stylesheet" href="assets/css/app.css">
  </head>
<body>  

<div class="container">
  <?php 
    $form = $wfApi->getForm();  
  ?>
  <h1><?php echo $form['Forms'][0]['Name'] ?></h1>
  
  <table id="entries_list" class="display responsive-table striped" style="width:100%"  border=1>  
    
  <?php 
    // Get Form Fields from API 
    $fields = $wfApi->getFields();
    
    // Define array and fill it with field titles
    $fieldsDisplay = [];
    foreach($fields['Fields'] as $field) {
      $fieldsDisplay[$field['ID']] = $field['Title'];
    }

    // Get Entries from API
    $entryId = "";
    if (isset($_GET['id'])) $entryId = $_GET['id'];
    $entries = $wfApi->getEntries($entryId); 
    
    // Create table header row
    foreach($entries['Entries'] as $entry) {
      echo "<thead><tr>";
      foreach ($entry as $key => $value ) {
        // Skip fields not in the fields display list (set in config)
        if (!in_array($key, $showFields)) continue;
        echo "<td>";
        echo $fieldsDisplay[$key];
        echo "</td>";
      } 
      break; 
      echo "</tr></thead>";
    }    
    echo "<tbody>";
    
    // Create row and columsn for each entry    
    foreach($entries['Entries'] as $entry) {
      echo "<tr>";
      foreach ($entry as $key => $value ) {
        // Skip fields not in the fields display list (set in config)
        if (!in_array($key, $showFields)) continue;
        
        echo "<td>";
        if (!in_array($key, $fileFields)) {
          echo $value;  
        } else {
          // Handle the links for files
          if ($value != "") {
            $file = substr($value, strpos($value, " (") + 2);  
            $file = substr($file, 0, -1);
            echo sprintf('<a href="%s" target="_blank">View</a>',$file);  
          }
        }
        echo "</td>";
      }  
      echo "</tr>";
    }
    echo "</tbody>";
  
  ?>
  
  </table>
    
    
</div> 
  
<?php if (isset($_GET['id'])) { ?>

  <div class="container center-align">
    <a href="index.php" class="btn">Show All</a>
  </div>

<?php } ?>
 

  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/jquery.dataTables.min.js"></script>
  <script src="assets/js/materialize.min.js"></script>
  <script src="assets/js/app.js"></script>

  
</body>

</html>


