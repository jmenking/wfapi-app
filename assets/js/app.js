$(document).ready( function () {
  
  $.extend( true, $.fn.dataTable.defaults, {
    "pageLength": 20,
    "lengthChange": false,
    "order": [[ 7, 'asc' ]],
  } );  

  $('#entries_list').DataTable({
    "pagingType": "numbers",
  });
  
  $('select').formSelect();
} );